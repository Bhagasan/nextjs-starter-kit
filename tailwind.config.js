/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    screens: {
      '2xs': '380px',
      xs: '540px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      xlm: '1366px',
      '2xl': '1600px',
      '3xl': '1920px',
    },
    fontFamily: {
      BMW: ['bmw', 'sans-serif'],
      'BMW-thin': ['bmw-thin', 'sans-serif'],
      'BMW-light': ['bmw-light', 'sans-serif'],
      'BMW-med': ['bmw-medium', 'sans-serif'],
      'BMW-bold': ['bmw-bold', 'sans-serif'],
    },
  },
  plugins: [],
};
