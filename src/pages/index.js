import Layout from 'components/Layout';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getProfile } from 'store/actions/user';

export default function Home() {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.user.me);

  useEffect(() => {
    dispatch(getProfile());
  }, [dispatch]);
  console.log(profile);
  return (
    <Layout>
      <div>Home</div>
    </Layout>
  );
}
