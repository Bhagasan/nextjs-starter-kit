import React from 'react';
import Nav from './Nav';

const Layout = ({ children }) => {
  return (
    <div
      className='fixed left-0 top-0 w-full h-[100svh] overflow-auto bg-cover bg-center'
      style={{ backgroundImage: 'url(/images/bg.png)' }}
    >
      <Nav />
      <div className='w-[375px] mx-auto px-6 pt-24'>{children}</div>
    </div>
  );
};

export default Layout;
