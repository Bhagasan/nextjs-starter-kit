import Image from 'next/image';
import React from 'react';

const Nav = () => {
  return (
    <div className='fixed w-[375px] left-1/2 top-0 -translate-x-1/2 flex justify-between items-start pt-4'>
      <div>x5</div>
      <Image src='/images/logo.png' width={80} height={80} alt='PROMILD' />
      <div>1000</div>
    </div>
  );
};

export default Nav;
