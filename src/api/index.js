import axios from 'axios';
import { API_BASE_URL } from 'helper/constant';

const API = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    'x-api-key': process.env.NEXT_PUBLIC_SECRET_KEY,
  },
});

API.interceptors.request.use((req) => {
  if (localStorage.getItem('profile')) {
    req.headers.Authorization = `Bearer ${
      JSON.parse(localStorage.getItem('profile')).accessToken
    }`;
  }
  return req;
});

export const getProfile = () => API.get('v1/auth/me');
