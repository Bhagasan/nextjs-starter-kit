const { GET_PROFILE } = require('store/types');

const initMe = {
  email: 'lalala@123.com',
};

const userReducer = (state = { me: initMe }, action) => {
  switch (action.type) {
    case GET_PROFILE:
      localStorage.setItem('p', JSON.stringify(action.data));
      return {
        ...state,
        me: action.data,
        loading: false,
      };
    default:
      return state;
  }
};

export default userReducer;
