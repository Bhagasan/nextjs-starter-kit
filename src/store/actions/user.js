import * as api from 'api';
import { GET_PROFILE } from 'store/types';

export const getProfile = () => async (dispatch) => {
  try {
    const { data } = await api.getProfile();
    dispatch({ type: GET_PROFILE, payload: { data } });
    return data.data;
  } catch (error) {
    throw error.response;
  }
};
